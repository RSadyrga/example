const countdown = require('future-count-down')


export function pluralize(number, one, two, five) {
  let n = Math.abs(number)
  n %= 100
  if (n >= 5 && n <= 20) {
    return five
  }
  n %= 10
  if (n === 1) {
    return one
  }
  if (n >= 2 && n <= 4) {
    return two
  }
  return five
}

function formatDate(value) {
  return value < 10 ? `0${value}` : value
}

const countdownEl = document.querySelector('.countdown')

if (countdownEl) {
  const daysTabEl = countdownEl.querySelector('#days')
  const hoursTabEl = countdownEl.querySelector('#hours')
  const minutesTabEl = countdownEl.querySelector('#minutes')
  const secondsTabEl = countdownEl.querySelector('#seconds')

  const daysValEl = daysTabEl.querySelector('.countdown__value')
  const daysTipEl = daysTabEl.querySelector('.countdown__tip')

  const hoursValEl = hoursTabEl.querySelector('.countdown__value')
  const hoursTipEl = hoursTabEl.querySelector('.countdown__tip')

  const minutesValEl = minutesTabEl.querySelector('.countdown__value')
  const minutesTipEl = minutesTabEl.querySelector('.countdown__tip')

  const secondsValEl = secondsTabEl.querySelector('.countdown__value')
  const secondsTipEl = secondsTabEl.querySelector('.countdown__tip')

  const dateDataArr = countdownEl.dataset.deadline.split('.').map(item => Number(item))
  const date = new Date()

  date.setDate(dateDataArr[0])
  date.setMonth(dateDataArr[1] - 1)
  date.setFullYear(dateDataArr[2])
  date.setHours(dateDataArr[3])
  date.setMinutes(dateDataArr[4])

  console.log('end date', new Date(date))
  
  countdown({
    endDate: new Date(date),
    interval: 1000,
    run: function({ days, hours, minutes, seconds }){
      daysValEl.textContent = formatDate(days)
      daysTipEl.textContent = pluralize(days, 'День', 'Дня', 'Дней')

      hoursValEl.textContent = formatDate(hours)
      hoursTipEl.textContent = pluralize(hours, 'Час', 'Часа', 'Часов')

      minutesValEl.textContent = formatDate(minutes)
      minutesTipEl.textContent = pluralize(minutes, 'Минута', 'Минуты', 'Минут')

      secondsValEl.textContent = formatDate(seconds)
      secondsTipEl.textContent = pluralize(seconds, 'Секунда', 'Секунды', 'Секунд')
    },
    finish(){
      //do something when finished
    }
  })
}
  