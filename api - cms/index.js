import Request from '@/utils/create-request.js'
import Response from '@/utils/api/response-handler'
import Types from '@/store/types'
import { mapCmsRequest } from '@/utils/api/map-request'

export default class CmsApi {
  @Response()
  static fetch({ subdomain }) {
    return Request.get(`/admincontroller/${subdomain}`, {
      params: {
        code: Types.cms
      }
    })
  }

  @Response()
  static async update({ page }) {
    return Request.post(
      '/admincontroller',
      { page: await mapCmsRequest(page) },
      {
        params: {
          page: Types.cms
        }
      }
    )
  }
}
